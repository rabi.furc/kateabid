var express = require("express");
var app = express();
var path = require('path');
var http = require("http").createServer(app);
var formidable = require("formidable");
var fs = require("fs");
var conn = require('./database/database')
var ObjectID = require('mongodb').ObjectID;

const session = require('express-session')
const passwordProtected = require('express-password-protect')
 
const config = {
    username: "kate",
    password: "dodvLgJM",
    maxAge: 60*60*60*1024 
}

var bodyParser = require("body-parser")
app.set("view engine", "ejs");

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // this is used for parsing the JSON object from POST
app.use(passwordProtected(config))
http.listen(4000, async function () {

	app.get("/", function (request, result) {

		result.render("index");

	});

	app.post("/home", function (request, result) {

		result.render("index");

	});

	

	app.post('/upload', function(req, res){


		
		var form = new formidable.IncomingForm(),
		files = [];

		var fieldObject = new Object();

		form.on('field', function(field, value1) {
			console.log(field, "===> field")
			fieldObject[String(field)] = value1;
		})


		form.on('file', function(field, file) {
			console.log(file.name);
			
			files.push([field, file]);
		})

		form.on('end', async function() {

			var db = await conn;

			var mongoID = new ObjectID();

			console.log(fieldObject, "===> fieldObject");

			console.log(files, "===> files")

			var fileNames = []
			
			files.forEach(aFile => {

				console.log(aFile[1].path, "===> aFile")

				var extension = aFile[1].name.substr((aFile[1].name.split("."))[0]);

				var fileName =  String(mongoID) + "_" + extension;

				var fileNameObject = new Object();
				fileNameObject["name"] = fileName;
				fileNameObject["nameOriginal"] = extension;

				fileNames.push(fileNameObject)

				var newPath = "public/uploads/" + String(mongoID) + "_" + extension;

				console.log(newPath, "===> newPath")

				fs.rename(aFile[1].path, newPath, function (errorRename) {

					console.log("File saved = " + newPath);

				});

			})

			var mongoInsert = new Object(fieldObject);
			mongoInsert["_id"] = mongoID;
			mongoInsert["fileNames"] = fileNames
			
			
			db.collection("entries").insertOne(mongoInsert);

			console.log('done');

			res.redirect('/');

		});

		form.parse(req);
	});


	app.get('/play/:fileName/:fileNameOriginal', async function(req, res) {

		var renderData = new Object();
		renderData["fileName"] = req.params.fileName;
		renderData["fileNameOriginal"] = req.params.fileNameOriginal
		renderData["link"] = "/uploads/"+req.params.fileName;

		res.render('play', renderData);
	})

	app.get('/download/:filename', function(req, res){
		console.log(__dirname)

		console.log("sadfasdfasdfasdfasdfasdfasdf")
		const file = __dirname + "/uploads/" +req.params.filename;
		res.download(file); // Set disposition and send it.
	});

	app.get('/filter', async function(req, res) {

		res.render("filter")
		

	})

	app.post("/filter", async function(req, res) {
		
		var postData = new Object(req.body);

		console.log(typeof req.body)

		var db = await conn;

		console.log(postData["firstname"], "===> postData")

		var mongoSearch = new Object();

		for (var key of Object.keys(postData)) {
				console.log(key + " -> " + postData[key]);
				mongoSearch[key] = {$regex :new RegExp(postData[key], "i")};
			
		}

		console.log(mongoSearch, "===> mongoSearch")

		
		var entriesData = await db.collection("entries").find(mongoSearch).toArray();

		res.send(entriesData)

	});


	app.post("/deleteRecord", async function(req, res) {
		
		var recordID = new ObjectID(req.body.id);

		console.log(req.body)

		var db = await conn;

		var mongoSearch = new Object();

		mongoSearch["_id"] = recordID;
		
		db.collection("entries").deleteOne(mongoSearch, async function(err, obj) {

			if(err){

				console.log("Error deleting record and err is: ", err);

				res.status(500).send({success: false, err: err});

			} else {

				res.status(200).send({success: true})

			}

		})

	})
	
});